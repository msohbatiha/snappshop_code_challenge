### Requirements :
- PHP >= 7.3 ;

### How To run
- Run this command in Terminal :
``` php ./Find.php ```

### Example response :
```
blocks list in order: (keys are blocks number and values are total calculated weight(lower is better)):
Array
(
    [3] => 2
    [2] => 4
    [4] => 4
    [1] => 10
    [0] => 17
)
Recommended block number(started from zero) : 3
```

### Change Blocks data :
- for change blocks data you can edit `./data.json` file.