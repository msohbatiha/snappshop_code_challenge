<?php

class Finder
{
    private $fileName;

    public function __construct($fileName)
    {
        $this->fileName = $fileName;
    }

    public function bestRecommendations($myFilters)
    {
        // read data from file and convert that to array
        $data = $this->getDataFromFile();

        /*
         * Find the active services in the blocks and categorize them in $services
         * keys are services name and values are array of blocks numbers
        */
        $categorize_services = $this->CategorizeServices($data, $myFilters);

        /*
         * calculate the nearest service distance for each block and save them in $distance_data
         * in $distance_data keys are blocks number and values are services name and distance
         *  */
        $distances_data = $this->calculateServicesDistanceForEachBlock($data, $categorize_services, $myFilters);

        /*
         we weigh the distances and finally calculate the total weight for each block
         and store them in $weighted_blocks .
         $weighted_blocks keys are blocks number and values are total weight
        */
        $weighted_blocks = $this->calculateWeightedDistances($distances_data, $myFilters);


        //sorting calculated weights in asc
        asort($weighted_blocks);

        //print calculated results :
        $this->printResults($weighted_blocks);
    }

    private function getDataFromFile()
    {
        $data = file_get_contents($this->fileName);
        return json_decode($data, true);
    }

    private function categorizeServices($data, $myFilters): array
    {
        $categories = [];
        foreach ($myFilters as $filter) {
            $categories[$filter] = array_keys(array_column($data, $filter), true);
        }
        return $categories;
    }

    private function calculateServicesDistanceForEachBlock($data, $categorize_services, $myFilters): array
    {
        $distance_data = [];
        foreach ($data as $index => $block) {
            foreach ($block as $service_name => $v) {
                if (!in_array($service_name, $myFilters)) {
                    continue;
                }
                $distance_array = [];

                foreach ($categorize_services[$service_name] as $b) {
                    $distance_array[] = abs($b - $index);
                }

                //nearest block
                $distance_data[$index][$service_name] = min($distance_array);
            }
        }
        return $distance_data;
    }

    private function calculateWeightedDistances($distance_data, $myFilters): array
    {
        $weighted_blocks = [];
        foreach ($distance_data as $index => $block) {
            $total_weight = 0;
            foreach ($myFilters as $service_name) {
                $total_weight += pow($block[$service_name], 2);
            }
            $weighted_blocks[$index] = $total_weight;
        }
        return $weighted_blocks;
    }

    private function printResults(array $weighted_blocks)
    {
        print_r('blocks list in order: (keys are blocks number and values are total calculated weight (lower is better))' . PHP_EOL);
        print_r($weighted_blocks);
        print_r('Recommended block number(started from zero) : ' . array_key_first($weighted_blocks) . PHP_EOL);

    }
}

$finder = new Finder('data.json');

$finder->bestRecommendations(['school', 'pool', 'supermarket']);
